        # Презентация #
# На тему: .copy(), .deepcopy() #
# import copy
# test_1 = [1, 2, 3, [1, 2, 3]]
# test_2 = test_1.copy()
# print("Test_1:", test_1)
# print("Test copy:", test_2)

# test_1[0] = 5
# test_1[3][1] = 8
# print("Test_1:", test_1)
# print("Test copy:", test_2)

# test_3 = [1, 2, 3, [1, 2, 3]]
# # print(dir(test_3))
# test_4 = copy.deepcopy(test_3)
# print("Test_3:", test_3)
# print("Test copy:", test_4)
#
# test_3[0] = 5
# test_3[3][1] = 8
# print("Test_3:", test_3)
# print("Test copy:", test_4)

# old_list = [[1, 1, 1], [2, 2, 2], [3, 3, 3]]
# new_list = copy.deepcopy(old_list)
#
# new_list[1][0] = 'BB'
#
# print("Old list:", old_list)
# print("New list:", new_list)