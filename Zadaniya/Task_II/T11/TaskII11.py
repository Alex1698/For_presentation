# string

l = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# 1
print(l[2])
# 2
print(l[-1])
# 3
print(l[:5])
# 4
print(l[:8])
# 5
l2 = []
for i in range(len(l)):
    if i%2 == 0:
        l2.append(l[i])
print(' '.join(map(str, l2)))
# 6
l3 = []
for i in range(len(l)):
    if i % 2 == 1:
        l3.append(l[i])
print(' '.join(map(str, l3)))
# 7
l4 = l[::-1]
print(l4)
# 8
l4 = []
for i in range(len(l)):
    if i%2 == 1:
        l4.append(l[i])
print(' '.join(map(str, l4[::-1])))
# 9
print(len(l))